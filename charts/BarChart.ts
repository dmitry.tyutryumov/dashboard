import * as d3 from "d3";
import { BaseChart, IChartProps } from "./base";

const ChartData = [
  {
    users: 850,
    sales: 1
  },
  {
    users: 600,
    sales: 2
  },
  {
    users: 500,
    sales: 3
  },
  {
    users: 620,
    sales: 4
  },
  {
    users: 900,
    sales: 5
  },
  {
    users: 500,
    sales: 6
  },
  {
    users: 900,
    sales: 7
  },
  {
    users: 630,
    sales: 8
  },
  {
    users: 1000,
    sales: 9
  }
];

interface IData {
  users: Number;
  sales: Number;
  [key: string]: Number;
}

export class BarChart extends BaseChart {
  g: any;
  x: any;
  y: any;
  xAxis: any;
  yAxis: any;
  data: IData[];

  constructor({
    selector,
    height = 500,
    width = 500,
    margin = null,
    data = null,
    colors = null
  }: IChartProps) {
    super({ selector, height, width, margin, data, colors });
    this.data = ChartData;
    this.tooltip = this.svg
      .append("g")
      .attr("class", "tooltip")
      .style("display", "none");
  }

  updateScales() {
    this.x.domain(this.data.map(row => row.sales));
    this.y.domain([0, d3.max(this.data.map(row => row.users))]);
    this.colors.domain([this.data.map(row => row.users)]);
  }

  drawXAxis() {
    super.drawXAxis();
    d3.selectAll(".xaxis .tick text")
      .attr("text-anchor", "end")
      .attr("transform", "translate(20 0)")
      .style("font-size", "1.7em")
      .attr("fill", "white");
  }

  drawYAxis() {
    super.drawYAxis();
    d3.selectAll(".yaxis .tick text")
      .attr("text-anchor", "end")
      .style("font-size", "1.7em")
      .attr("fill", "white");
  }

  drawChart() {
    this.g
      .selectAll("rect")
      .data(this.data)
      .join("rect")
      .attr("class", "bar")
      .attr("x", (d: IData) => this.x(d.sales) - 10)
      .attr("y", (d: IData) => this.y(d.users))
      .attr("rx", 6)
      .attr("ry", 6)
      .attr("width", 20)
      .attr("height", (d: IData) => Number(this.height) - this.y(d.users))
      .style("fill", "white")
      .transition()
      .duration(100);
  }

  drawTooltip() {
    this.tooltip
      .append("rect")
      .attr("height", 50)
      .attr("width", 90)
      .attr("x", 20)
      .attr("y", 20)
      .attr("rx", 6)
      .attr("ry", 6);

    this.tooltip
      .append("rect")
      .attr("height", 15)
      .attr("width", 15)
      .attr("x", 25)
      .attr("y", 45)
      .attr("fill", "white");

    this.g
      .selectAll(".bar")
      .on("mouseenter", (event: Event, d: IData) => {
        this.tooltip.style("display", "block");
        let x = this.x(d.sales) + 60;
        let y = this.y(d.users) - 20;
        this.tooltip.attr("transform", `translate(${x} ${y})`);
        this.tooltip.selectAll("text").remove();

        this.tooltip
          .append("text")
          .text("0" + d.sales)
          .attr("x", 25)
          .attr("y", 40)
          .attr("fill", "white")
          .style("font-size", ".9em");

        this.tooltip
          .append("text")
          .text("Sales: " + d.users)
          .attr("x", 45)
          .attr("y", 60)
          .attr("fill", "white")
          .style("font-size", ".9em");
      })
      .on("mouseout", () => this.tooltip.style("display", "none"));
  }
}
