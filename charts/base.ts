import * as d3 from "d3";

export interface IMargin {
  top: Number;
  bottom: Number;
  left: Number;
  right: Number;
}

export interface IChart {
  margin: IMargin;
  width: Number;
  height: Number;
  svg: d3.Selection<SVGSVGElement, unknown, HTMLElement, any>;
  g: any;
  x: any;
  y: any;
  colors: any;
  xAxis: any;
  yAxis: any;
  data: any[];

  draw: () => void;
  addX: () => void;
  addY: () => void;
  addColors: () => void;
  addXAxisTitle: () => void;
  addYAxisTitle: () => void;
  drawXAxis: () => void;
  drawYAxis: () => void;
  drawChart: () => void;
  drawTooltip: () => void;
  updateScales: () => void;
}

export interface IChartProps {
  selector: string;
  height?: Number;
  width?: Number;
  margin?: IMargin | null;
  data?: any;
  colors?: string[] | null;
}

export class BaseChart implements IChart {
  margin: IMargin;
  width: Number;
  height: Number;
  svg: d3.Selection<SVGSVGElement, unknown, HTMLElement, any>;
  g: any;
  tooltip: any;
  x: any;
  y: any;
  colors: any;
  xAxis: any;
  yAxis: any;
  data: any[];

  constructor({
    selector,
    height = 500,
    width = 500,
    margin = null,
    data = null,
    colors = null
  }: IChartProps) {
    this.margin = margin
      ? margin
      : { left: 70, right: 10, top: 30, bottom: 40 };
    this.height =
      Number(height) - Number(this.margin.top) - Number(this.margin.bottom);
    this.width =
      Number(width) - Number(this.margin.left) - Number(this.margin.right);
    this.data = data;
    this.colors = [];
    this.tooltip = null;

    // selects element & add svg
    const viewBoxHeight =
      Number(this.height) +
      Number(this.margin.top) +
      Number(this.margin.bottom);

    const viewBoxWidth =
      Number(this.width) + Number(this.margin.right) + Number(this.margin.left);

    this.svg = d3
      .select(selector)
      .append("svg")
      .attr("preserveAspectRatio", "xMinYMid meet")
      .attr("viewBox", `0 0 ${viewBoxWidth} ${viewBoxHeight}`);

    // adds root g container
    this.g = this.svg
      .append("g")
      .attr("transform", `translate(${this.margin.left}, ${this.margin.top})`);

    // adds scales
    this.addX();
    this.addY();
    this.addColors(colors);

    this.g.append("g").attr("class", "axis yaxis");
    this.g
      .append("g")
      .attr("class", "axis xaxis")
      .attr("transform", `translate(0, ${this.height})`);

    // starts drawing
    this.addXAxisTitle();
    this.addYAxisTitle();
  }

  draw() {
    // add domains to x/y/colors scales
    this.updateScales();
    this.drawXAxis();
    this.drawYAxis();
    this.drawChart();
    this.drawTooltip();
  }

  addX() {
    this.x = d3
      .scaleBand()
      .range([0, Number(this.width)])
      .padding(1);
  }
  addY() {
    this.y = d3.scaleLinear().range([Number(this.height), 0]);
  }
  addColors(colors: string[] | null = null) {
    this.colors = d3.scaleOrdinal(colors ? colors : d3.schemeCategory10);
  }
  addXAxisTitle() {}
  addYAxisTitle() {}
  drawXAxis() {
    this.xAxis = d3.axisBottom(this.x).tickFormat(d => `0${d}`);
    this.g
      .select(".xaxis")
      .transition()
      .call(this.xAxis);
    d3.selectAll(".xaxis .tick text")
      .attr("y", 10)
      .attr("x", -10);
  }
  drawYAxis() {
    this.yAxis = d3.axisLeft(this.y).ticks(6);
    this.g.select(".yaxis").call(this.yAxis);
    d3.selectAll(".yaxis .tick text")
      .attr("y", 10)
      .attr("x", -10);
  }
  updateScales() {}
  drawChart() {}
  drawTooltip() {}
}
