import * as d3 from "d3";
import { BaseChart, IChartProps } from "./base";

const ChartData = [
  [
    { date: "2020-04-01", value: 50 },
    { date: "2020-04-26", value: 25 },
    { date: "2020-05-09", value: 99 },
    { date: "2020-06-01", value: 300 },
    { date: "2020-07-01", value: 230 },
    { date: "2020-08-01", value: 500 },
    { date: "2020-09-01", value: 280 },
    { date: "2020-10-01", value: 400 },
    { date: "2020-11-01", value: 250 },
    { date: "2020-12-01", value: 490 }
  ],
  [
    { date: "2020-04-01", value: 70 },
    { date: "2020-04-26", value: 80 },
    { date: "2020-05-09", value: 88 },
    { date: "2020-06-01", value: 50 },
    { date: "2020-07-01", value: 120 },
    { date: "2020-08-01", value: 300 },
    { date: "2020-09-01", value: 280 },
    { date: "2020-10-01", value: 350 },
    { date: "2020-11-01", value: 250 },
    { date: "2020-12-01", value: 400 }
  ]
];

interface IData {
  date: String;
  value: Number;
  [key: string]: Number | String;
}

export class LineChart extends BaseChart {
  g: any;
  x: any;
  y: any;
  xAxis: any;
  yAxis: any;
  data: IData[][];

  constructor({
    selector,
    height = 500,
    width = 500,
    margin = null,
    data = null,
    colors = null
  }: IChartProps) {
    super({ selector, height, width, margin, data, colors });
    this.data = ChartData;
  }

  drawChart() {
    this.data.forEach((row, i) => {
      this.g
        .append("path")
        .datum(row)
        .attr("fill", "none")
        .attr("stroke", this.colors(i))
        .attr("stroke-width", 1.5)
        .attr(
          "d",
          d3
            .line()
            .curve(d3.curveNatural)
            .x((d: any) => this.x(this._formatTime()(String(d.date))))
            .y((d: any) => this.y(d.value))
        );

      this.g
        .selectAll("rect")
        .data(row)
        .join("circle")
        .attr("class", "bar")
        .attr("cx", (d: IData) => this.x(this._formatTime()(String(d.date))))
        .attr("cy", (d: IData) => this.y(d.value))
        .attr("r", 5)
        .style("fill", this.colors(i))
        .transition()
        .duration(1000);
    });
  }

  updateScales() {
    let dateRange = this.data[0].map(row =>
      this._formatTime()(String(row.date))
    ) as Date[];
    this.x.domain(d3.extent(dateRange));
    this.y.domain([0, 500]);
    this.colors.domain([0, 1]);
  }

  addX() {
    this.x = d3.scaleTime().range([0, Number(this.width)]);
  }

  drawXAxis() {
    this.xAxis = d3
      .axisBottom(this.x)
      .tickFormat((d: any) => d.toLocaleString("default", { month: "short" }));
    this.g
      .select(".xaxis")
      .transition()
      .call(this.xAxis);

    this.g.selectAll(".xaxis .tick line").style("visibility", "hidden");
    // this.g.select(".xaxis .tick:first-child").remove();

    this.g
      .selectAll(".xaxis .tick text")
      .attr("y", 10)
      .attr("x", -10)
      .attr("text-anchor", "start")
      .attr("transform", "translate(0 0)")
      .style("font-size", "1em")
      .attr("fill", "black");
  }

  drawYAxis() {
    this.yAxis = d3.axisLeft(this.y).ticks(6);
    this.g.select(".yaxis").call(this.yAxis);
    this.g
      .selectAll(".yaxis .tick text")
      .attr("y", 10)
      .attr("x", -10)
      .attr("text-anchor", "end")
      .style("font-size", "1em")
      .attr("fill", "black");
  }

  drawTooltip() {}

  _formatTime() {
    return d3.timeParse("%Y-%m-%d");
  }
}
