interface State {
  showSideMenu: boolean;
}

export const state = () => ({
  showSideMenu: false
});

export const mutations = {
  hideSideMenu(state: State) {
    state.showSideMenu = false;
  },
  openSideMenu(state: State) {
    state.showSideMenu = true;
  }
};

export const getters = {
  getSideMenuState(state: State) {
    return state.showSideMenu;
  }
};
