export const capitalize = (value: string): string => {
  return value.charAt(0).toUpperCase() + value.substring(1);
};

export const getDiffColor = (diff: string): string => {
  return diff.startsWith("+") ? "text-green-500" : "text-red-500";
};
